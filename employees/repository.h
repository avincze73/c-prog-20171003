/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   repository.h
 * Author: avincze
 *
 * Created on 2017. október 4., 15:32
 */

#ifndef REPOSITORY_H
#define REPOSITORY_H

typedef struct {
    char name[50];
    char title[50];
    double salary;
} Employee; 

void insert(const Employee*);
void show();
#endif /* REPOSITORY_H */
