/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   repository.c
 * Author: avincze
 * 
 * Created on 2017. október 4., 15:32
 */

#include "repository.h"
#include <string.h>
int top = 0;
Employee database[100];

void insert(const Employee* employee)
{
    //Employee emp = {.salary = employee->salary, .name = employee->name};
    
    Employee emp = {.salary = employee->salary};
    strcpy(emp.name, employee->name);
    strcpy(emp.title, employee->title);
    database[top++] = emp;
}

void show()
{
    for(int i = 0; i < top; ++i)
    {
        printf("Name: %s\nTitle: %s\nSalary:%f\n", 
                database[i].name, database[i].title, database[i].salary);
    }
}


