/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2017. október 4., 11:03
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 * 
 */

int accum(int* a, int length, int init) {
    for (int i = 0; i < length; ++i) {
        init += a[i];
    }
    return init;
}

int accum_1(int* a, int length, int init, int (*operation)(int, int)) {
    for (int i = 0; i < length; ++i) {
        init = operation(init, a[i]);
    }
    return init;
}

int accum_2(int* begin, int* end,
        int init,
        int (*operation)(int, int),
        bool(*predicate)(int)) {
    for (; begin != end; ++begin) {
        if (predicate(*begin)) {
            init = operation(init, *begin);
        }
    }
    return init;
}

int Add(int a, int b) {
    return a + b;
}

int Multiply(int a, int b) {
    return a * b;
}

bool Even(int a)
{
    return a%2 == 0;
}
int main(int argc, char** argv) {
    int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    printf("%d\n", accum(a, 10, 0));
    printf("%d\n", accum_1(a, 10, 0, Add));
    printf("%d\n", accum_1(a, 10, 1, Multiply));
    
    printf("%d\n", accum_2(a, a+10, 0, Add, Even));
    
    return (EXIT_SUCCESS);
}

